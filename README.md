## Rfider assessment - Back end

Done by: Diego Evangelisti

# Running Instructions
```sh
$ npm install
$ npm run serve
``` 

# Build Instructions
```sh
$ npm run lint
$ npm run build
``` 

# Performance issue
When opening "http://127.0.0.1:3000/export" you will notice that there is a blocking script which can cause the Node process to run out or heap memory.

# Solution
- Make this a non blocking script. - (eg: use of worker threads). Please be ready to explain the different alternatives.

## Part 1

### GET  /export-worker

**URL:** `http://127.0.0.1:3000/export-worker`

**Description:** This method uses a child worker thread to generate the array with all the numbers. Once ready, the worker retrieves the results to the main thread, and then the response object is created and sent to the client.

## Part 2

- Refactor to use Node Streams to output response and prevent memory errors.

### GET  /export-streams 

**URL:** `http://127.0.0.1:3000/export-streams`


**Description:** In this case, with the use of streams, readStream takes all the generated numbers in small portions. Then, we pipe that to the response object. Doing so, the data is sent to the client limiting the buffering of data to acceptable levels.
