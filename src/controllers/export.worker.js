const { parentPort, workerData } = require('worker_threads');

const segment = workerData

let start = segment * 1000000 
const end = (segment + 1) * 1000000
let items = ''

while (start < end) {
  items+=`${start},`
  start += 1;
};

parentPort.postMessage(items);
