import { Router, Request, Response } from 'express';
import IControllerBase from '../interfaces/IControllerBase.interface';
import { OK } from 'http-status-codes';
import Stream from 'stream';
const { Worker } = require('worker_threads');
const path = require('path');
const workerPath = path.resolve(__dirname, 'export.worker.js')


class Controller implements IControllerBase {
  public router = Router();

  constructor() {
    this.initRoutes();
  }

  public initRoutes() {
    this.router.get('/export', this.export)
    this.router.get('/export-worker', this.exportWorker)
    this.router.get('/export-streams', this.exportStreams)
  }

  export = async (request: Request, response: Response) => {

    let start = 1;
    const end = 7000000
    let items: number[] = [];

    while (start < end) {
      items.push(start);
      start += 1;
    };
    response.status(OK).send(items)
  }

  exportWorker = async (request: Request, response: Response) => {

    response.connection.on('close', () => {
      console.log('Connection closed')
    });

    const readableStream = new Stream.Readable();
    const segments: number[] = [0, 1, 2, 3, 4, 5, 6]

    let promises = segments.map(segment => new Promise((resolve, reject) => {
      const worker = new Worker(workerPath, { workerData: segment });
      worker.on('message', resolve);
      worker.on('error', reject);
      worker.on('exit', (code: number) => {
        if (code !== 0) reject(new Error(`Worker stopped with exit code ${code}`));
      });
    }));

    Promise.all(promises).then(results => {
      readableStream.push(results.toString());
      readableStream.push(null);
      return readableStream.pipe(response);
    });
  }

  exportStreams = async (request: Request, response: Response) => {

    const readableStream = new Stream.Readable();

    response.connection.on('close', () => {
      console.log('Connection closed')
    });

    const end = 7000000;
    for (let start = 1; start < end; ++start) {
      readableStream.push(`${start}`)
    }
    readableStream.push(null);
    readableStream.pipe(response);

    readableStream.on('end', () => {
      console.log('There will be no more data.');
    });
  }
}

export default Controller;
