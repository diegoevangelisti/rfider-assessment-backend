interface IExceptionBase {
  status: number;
  message: string;
}

export default IExceptionBase;
