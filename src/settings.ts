const settings = {
  prefix: process.env.API_PREFIX || '/',
  port: process.env.PORT || 3000,
};

export { settings };
